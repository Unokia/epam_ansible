Для изменения существующей строки (добавление параметров net.ifnames=0 и biosdevname=0) в /etc/default/grub на текущем этапе не удалось применить lineinfile. Либо с регулярками не разобрался, либо lineinfile не применим для редактирования подстрок, а служит только для добавления/замены/удаления строк целиком.
Решил использовать replace. У replace тоже вышли подводные камни. Поскольку по-умолчанию требуемые параметры (net.ifnames=0 и biosdevname=0) уже присутвуют в строке, то добавил проверку на их наличие (просто через grep), которая является условием для запуска replace. Но итоге, replace у меня добавляет параметры в двух случаях: если нет таких параметров и если они есть тоже добавляет. Пробовал менять условие (в when, см.плейбук) с "!=" на "=", но ловлю ошибку вида:

fatal: [node3.example.com]: FAILED! => {"msg": "The conditional check 'check_ifnames = \"\"' failed.
The error was: template error while templating string:expected token 'end of statement block', got '='.
String: {% if check_ifnames = \"\" %} True {% else %} False {% endif %}\n\n
The error appears to be in '/home/ansible/lesson1/net.yml': line 14, column 11, but may\nbe elsewhere in the file depending on the exact syntax problem.
\n\nThe offending line appears to be:\n\n\n        - name: Insert_net.ifnames=0\n          ^ here\n"}

надеюсь разберусь с ней ко второй д/з, чтобы плейбук работал корректно при любых условиях.

